<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    // for function defualt is public
    public function index($id) {

        // add some actions
        
        // return "id : " . $id;

        // return response()->json ([
        //     'result' => [
        //         'id'=> $id
        //     ]
        // ]);

        // $sum = $id + 5;
        // return view('users.user')->with('sum', $sum);

        $arr = [
            'id' => 2,
            'firstname' => 'Albert',
            'lastname' => 'Einstein',
            'skills' => [
                'thai' => 100,
                'english' => 50
            ]
        ];
        // die();
        
        // dd($arr); // dd function is dump die
        return view('users.user')->with('arr', $arr);

    }
}
