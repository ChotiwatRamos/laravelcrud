@extends('layouts.master')
@section('title', 'Edit')
@section('content')
    <form action="{{ url('people', [$people->id]) }}" method="POST">
        @csrf
        @method('PUT')
        <h1>Edit People</h1>
        <div class="form-group">
            <label>Firstname</label>
            <input type="text" value="{{ $people->firstname }}" class="form-control" name="firstname">  
        </div>
        <div class="form-group">
            <label>Lastname</label>
        <input type="text" value="{{ $people->lastname }}" class="form-control" name="lastname">
            
        </div>
        <div class="form-group">
            <label>Age</label>
            <input type="text" value="{{ $people->age }}" class="form-control" name="age">
        </div>
        <button type="submit" class="btn btn-success">Save</button>
        @if ($errors->any())
        <div class="alert alert-danger mt-3 pt-4">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </form>
@endsection
@section('create-btn')
@endsection
