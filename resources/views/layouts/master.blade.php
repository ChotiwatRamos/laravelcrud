<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    @section('css')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    @show
    <title>@yield('title')</title>
</head>
<body>
    <div class="container">
    @include('layouts.navbar')
        @yield('content')
    @include('layouts.footer')
    @section('create-btn')
    <a href="{{ url('people/create') }}">
        <button class="btn btn-primary">CREATE</button>
    </a>
    @show
</div>
</body>
</html>



















{{-- <link rel="stylesheet" href="{{ asset('css/main.css') }}"> --}}

{{-- if (isset($id)) {
    echo $id;
} else {
    echo "Not found";
}

@if (isset($id))
{{ $id }}
@else 
Not Found
@endif

{{ isset($id) ? $id : 'Not Found' }}

{{ $id ?? 'Not Found' }}

@foreach ([1,2,3,4,5] as $item) 
{{ $item }}
@endforeach --}}