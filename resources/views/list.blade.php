@extends('layouts.master')
@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
@endsection

@section('title', 'List')

@section('content')
    @if (Session::has('message'))
    <div class="alert alert-success">
        {{ Session::get('message') }}
    </div>
    @endif
    <table class="table table-light table-striped text-center">
        <thead class="thead-dark">
            <th>ID</th>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Age</th>
            <th>Created_at</th>
            <th>Updated_at</th>
            <th>Actions</th>
        </thead>
        <tbody>
            @foreach ($people as $p)
            <tr>
                <td>{{ $p->id }}</td>
                <td>{{ $p->firstname }}</td>
                <td>{{ $p->lastname }}</td>
                <td>{{ $p->age }}</td>
                <td>{{ $p->created_at }}</td>
                <td>{{ $p->updated_at }}</td>
                <td>
                <div class="form-inline justify-content-center">
                <a href="{{ url('people/' . $p->id . '/edit') }}">
                    <button class="btn btn-warning">EDIT</button>
                </a>
                <form action="{{ url('people/'. $p->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger ml-1">DELETE</button>
                </form>
                </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection